/* eslint-disable require-jsdoc */

const memo = new Map();

function ropeCut(a, b, c, total) {
  if (total === 0) {
    return 0;
  }
  if (total < 0) {
    return -1;
  }

  if (memo.has(total)) {
    // console.log('Value from memo.');
    return memo.get(total);
  }

  // eslint-disable-next-line max-len
  const res = Math.max(ropeCut(a, b, c, total - a), ropeCut(a, b, c, total - b), ropeCut(a, b, c, total - c));

  if (res == -1) {
    memo.set(total, -1);
    return -1;
  }
  memo.set(total, res+1);
  return res + 1;
}

module.exports=ropeCut;
